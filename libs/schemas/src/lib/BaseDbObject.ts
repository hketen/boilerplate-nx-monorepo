import { Transform } from 'class-transformer';
import { IsOptional } from 'class-validator';
import { TimeStamps } from '@typegoose/typegoose/lib/defaultClasses';
import { Condition } from 'mongodb';

export class BaseDBObject extends TimeStamps {
  // this will expose the _id field as a string
  // and will change the attribute name to `id`
  @Transform((value) => value && value.toString())
  @IsOptional()
  _id?: any;

  [key: string]: any | Condition<any>;
}
