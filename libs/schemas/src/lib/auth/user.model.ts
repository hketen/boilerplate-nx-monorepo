import { IsEmail, IsString } from 'class-validator';
import { arrayProp, prop } from '@typegoose/typegoose';
import { UserEntity } from '@boilerplate-nx-monorepo/entities';
import { Transform } from 'class-transformer';
import { BaseDBObject } from '../BaseDbObject';

export class User extends BaseDBObject implements UserEntity {
  @Transform((value) => value && value.toString())
  _id: string;

  @IsString()
  @prop({ trim: true })
  fullName: string;

  @IsEmail()
  @prop({ required: true, unique: true, trim: true })
  email: string;

  @prop({ required: true, trim: true })
  password: string;

  @prop()
  profileImage: string;

  @arrayProp({ type: String })
  roles: string[];

  public get id_str(): string | null {
    return this._id && this._id.toString();
  }

  public toJSON() {
    return {
      _id: this._id,
      fullName: this.fullName,
      email: this.email,
      profileImage: this.profileImage,
      roles: this.roles,
    };
  }
}
