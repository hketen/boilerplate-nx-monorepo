import { AccountEntity } from '@boilerplate-nx-monorepo/entities';
import { BaseDBObject } from '../BaseDbObject';
import { prop } from '@typegoose/typegoose';

export class Account extends BaseDBObject implements AccountEntity {
  _id?: string;

  @prop()
  identificationNumber?: string;

  @prop()
  taxNumber?: string;

  @prop()
  taxOffice?: string;

  @prop()
  title: string;

  @prop()
  phoneNumber?: string;

  @prop()
  address?: string;

  @prop()
  explanation?: string;
}
