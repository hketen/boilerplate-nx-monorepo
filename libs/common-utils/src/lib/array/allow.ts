/**
 * use: allow(['hkn'], 'hkn') -> true;
 * use: allow(['hkn'], 'test') -> false;
 * use: allow([], 'test') -> true;
 *
 * @param arr
 * @param text
 */
export const allow = (arr: string[] = [], text: string) => {
  return arr && arr.length ? arr.includes(text) : true;
};
