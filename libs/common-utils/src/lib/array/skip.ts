/**
 * use: allow(['hkn'], 'hkn') -> false;
 * use: allow(['hkn'], 'test') -> true;
 * use: allow([], 'test') -> true;
 *
 * @param arr
 * @param text
 */
export const skip = (arr: string[] = [], text: string) => {
  return arr && arr.length ? !arr.includes(text) : true;
};
