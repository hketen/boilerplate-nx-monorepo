/**
 * use: allow(['hkn'], 'hkn') -> true;
 * use: allow(['hkn'], 'test') -> false;
 * use: allow([], 'test') -> true;
 *
 * @param arr
 * @param text
 */
export const search = (arr: string[] = [], text: string) => {
  return arr && arr.length
    ? arr.filter((s) => {
        const match = text.match(new RegExp(s, 'gi'));
        return match && match.length;
      }).length
    : true;
};
