import { toPretty } from '../converters/pretty';

/**
 * use: prettyRangeTime(10, 15, 5) -> ['10 minute', '15 minute'];
 *
 * @param time
 */
export const prettyRangeTimeValue = (
  time: number
): { value: number; text: string } => {
  return {
    value: time,
    text: toPretty(time * 60 * 1000),
  };
};
