import { range } from 'lodash';
import { toPretty } from '../converters/pretty';

/**
 * use: prettyRangeTime(10, 15, 5) -> ['10 minute', '15 minute'];
 *
 * @param min
 * @param max
 * @param step
 */
export const prettyRangeTime = (
  min: number,
  max: number,
  step: number
): { value: number; text: string }[] => {
  return range(min, max, step).map((t) => {
    const ms = t * 60 * 1000;

    return {
      value: t,
      text: toPretty(t * 60 * 1000),
    };
  });
};
