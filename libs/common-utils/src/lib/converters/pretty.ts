import ms from 'ms';

/**
 * use: prettyRangeTime(172000) -> 2 minutes 52 seconds
 *
 * @param time
 */
export const toMs = (time: string): number => {
  return ms(time, { long: true });
};

export const toPretty = (time: number): string => {
  return ms(time, { long: true });
};
