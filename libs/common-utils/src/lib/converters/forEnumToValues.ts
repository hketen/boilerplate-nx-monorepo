export const forEnumToValues = (enumObject) => {
  return Object.keys(enumObject).map((key: string) => enumObject[key]);
};
