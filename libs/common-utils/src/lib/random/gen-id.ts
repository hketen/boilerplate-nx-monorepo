const randomText = () => Math.random().toString(36).substr(2, 10);

export const genID = (length = 8) => {
  let random = randomText();

  while (random.length < length) {
    random += randomText();
  }

  return random.substring(0, length);
};
