const hex = () => (Math.round(Math.random() * 127) + 127).toString(16);

export const pastelColor = () => {
  const [r, g, b] = [hex(), hex(), hex()];

  return '#' + r + g + b;
};
