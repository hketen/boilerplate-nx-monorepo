export const isWithRoles = (
  permittedRoles: string[] = [],
  userRoles: string[] = []
) => {
  if (!permittedRoles.length) {
    return true;
  }

  if (userRoles.includes('root')) {
    return true;
  }

  return permittedRoles.some((role) => userRoles.includes(role));
};
