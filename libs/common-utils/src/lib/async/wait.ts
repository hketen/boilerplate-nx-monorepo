/**
 * use: await wait(10);
 *
 * @param seconds
 */
export const wait = (seconds = 1) => {
  return new Promise<void>((resolve) =>
    setTimeout(() => resolve, seconds * 1000)
  );
};
