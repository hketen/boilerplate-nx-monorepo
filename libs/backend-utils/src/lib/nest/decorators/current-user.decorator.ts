import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const CurrentUser = createParamDecorator(async (field: string, context: ExecutionContext) => {

  const { user } = context.switchToHttp().getRequest();

  return user
    ? field ? user[field] : user
    : null;
});
