import { applyDecorators, UseGuards } from '@nestjs/common';
import { JwtAuthGuard, RolesGuard } from '../guards';

export const Auth = (...roles: string[]) => {
  return applyDecorators(
    UseGuards(JwtAuthGuard),
    UseGuards(new RolesGuard(roles))
  );
};
