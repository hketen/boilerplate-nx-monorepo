import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const AuthToken = createParamDecorator(async (field: string, context: ExecutionContext) => {

  const { headers: { authorization } } = context.switchToHttp().getRequest();

  return authorization && authorization.split(' ')[1];
});
