import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class RolesGuard implements CanActivate {


  constructor(private readonly roles: string[]) {
    // private roles.
  }

  canActivate(
    context: ExecutionContext
  ): boolean | Promise<boolean> | Observable<boolean> {

    // TODO ???

    // const req = context.switchToHttp().getRequest();
    //
    // if (req.user && req.user.roles && req.user.roles.length) {
    //
    //   req.user.roles.filter((role) => this.roles.includes(role)).length
    // } else {
    //   return false;
    // }

    return true;
  }
}
