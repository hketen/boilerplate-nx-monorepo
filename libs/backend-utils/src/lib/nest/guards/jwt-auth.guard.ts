import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class JwtAuthGuard implements CanActivate {

  constructor(
    private readonly jwtService: JwtService
  ) {
  }

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {

    const req = context.switchToHttp().getRequest();

    const { headers: { authorization } } = req;
    const token = authorization && authorization.split(' ')[1];

    try {

      return req.user = this.jwtService.verify(token);
    } catch (e) {

      throw new UnauthorizedException();
    }
  }
}
