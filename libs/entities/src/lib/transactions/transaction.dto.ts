import { AccountDto } from '../accounts/account.dto';
import { TransactionTypeDto } from '../transaction-types/transaction-type.dto';

export interface TransactionDto<TAccount = AccountDto, TType = TransactionTypeDto> {

  readonly _id?: string;
  readonly account?: string | TAccount;
  readonly transactionType?: string | TType;
  readonly closed?: boolean;
  readonly balance?: number;
}
