import { IsBoolean, IsDate, IsNumber, IsOptional, IsString } from 'class-validator';
import { Transform } from 'class-transformer';
import { TransactionDetailDto } from './transaction-detail.dto';
import { TransactionEntity } from './transaction.entity';

export class TransactionDetailEntity<TTransaction = TransactionEntity> implements TransactionDetailDto<TTransaction> {

  @IsString()
  @IsOptional()
  @Transform((_id) => _id && _id.toString())
  _id?: string;

  @IsString()
  transaction?: string | TTransaction;

  @IsNumber()
  direction: 1 | -1; // 1 = Alacak | -1 = Borç

  @IsNumber()
  amount: number;

  @IsDate()
  @IsOptional()
  expiryDate?: Date;

  @IsBoolean()
  @IsOptional()
  completed?: boolean;

  // qty: number
}

