import { IsBoolean, IsMongoId, IsNumber, IsObject, IsOptional, IsString } from 'class-validator';
import { Transform } from 'class-transformer';
import { TransactionDto } from './transaction.dto';
import { AccountEntity } from '../accounts/account.entity';
import { TransactionTypeEntity } from '../transaction-types/transaction-type.entity';

export class TransactionEntity<TAccount = AccountEntity, TType = TransactionTypeEntity> implements TransactionDto<TAccount, TType> {

  @IsString()
  @IsOptional()
  @Transform((_id) => _id && _id.toString())
  _id?: string;

  @IsMongoId()
  account: string | TAccount;

  @IsMongoId()
  transactionType: string | TType;

  @IsObject()
  @IsOptional()
  customValues?: Map<string, any>;

  @IsBoolean()
  @IsOptional()
  closed?: boolean;

  @IsNumber()
  @IsOptional()
  total_credit?: number;

  @IsNumber()
  @IsOptional()
  total_debt?: number;

  @IsNumber()
  @IsOptional()
  total_balance?: number;
}

