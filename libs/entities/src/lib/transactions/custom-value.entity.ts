import { CustomValueDto } from './custom-value.dto';

export class CustomValueEntity implements CustomValueDto {

  [inputKey: string]: any;
}

