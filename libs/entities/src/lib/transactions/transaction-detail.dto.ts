import { TransactionDto } from '@boilerplate-nx-monorepo/entities';

export interface TransactionDetailDto<TTransaction = TransactionDto> {

  readonly _id?: string;
  readonly transaction?: string | TTransaction;
  readonly direction?: 1 | -1; // 1 = Alacak | -1 = Borç
  readonly amount?: number;
  readonly expiryDate?: Date;
  readonly completed?: boolean;
}
