export interface CustomValueDto {

  readonly [inputKey: string]: any;
}
