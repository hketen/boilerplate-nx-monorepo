import { IsNumber, IsString } from 'class-validator';

export class ImageEntity {

  @IsString()
  createdTime: string;

  @IsString()
  originalname: string;

  @IsString()
  filename: string;

  @IsString()
  mimetype: string;

  @IsNumber()
  size: number;
}
