export interface AccountDto {

  readonly _id?: string;
  readonly taxOffice?: string;
  readonly taxNumber?: string;
  readonly identificationNumber?: string;
  readonly title?: string;
  readonly phoneNumber?: string;
  readonly address?: string;
  readonly explanation?: string;
}
