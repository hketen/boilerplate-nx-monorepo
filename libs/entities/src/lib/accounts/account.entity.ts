import { IsOptional, IsString } from 'class-validator';
import { Transform } from 'class-transformer';
import { AccountDto } from './account.dto';

export class AccountEntity implements AccountDto {

  @IsString()
  @IsOptional()
  @Transform((_id) => _id && _id.toString())
  _id?: string;

  @IsString()
  @IsOptional()
  taxOffice?: string;

  @IsString()
  @IsOptional()
  taxNumber?: string;

  @IsString()
  @IsOptional()
  identificationNumber?: string;

  @IsString()
  title: string;

  @IsString()
  @IsOptional()
  phoneNumber?: string;

  @IsString()
  @IsOptional()
  address?: string;

  @IsString()
  @IsOptional()
  explanation?: string;

}

