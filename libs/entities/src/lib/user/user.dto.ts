export interface UserDto {

  readonly userId?: string,
  readonly fullName?: string,
  readonly email?: string,
  readonly password?: string,
  readonly profileImage?: string,
  readonly roles?: string[],
}
