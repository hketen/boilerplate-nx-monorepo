import { IsEmail, IsString } from 'class-validator';

export class UserEntity {

  @IsString()
  _id: string;

  @IsString()
  fullName: string;

  @IsEmail()
  email: string;

  @IsString()
  password: string;

  @IsString({ each: true })
  roles: string[];
}
