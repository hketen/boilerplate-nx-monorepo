import { IsString } from 'class-validator';
import { CustomInputDto } from './custom-input.dto';

export class CustomInputEntity implements CustomInputDto {

  @IsString()
  inputType!: string;

  @IsString()
  inputKey!: string;

  @IsString()
  label!: string;
}

