export interface CustomInputDto {

  readonly inputType?: string;
  readonly inputKey?: string;
  readonly label?: string;
}
