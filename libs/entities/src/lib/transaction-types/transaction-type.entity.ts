import { IsOptional, IsString, ValidateNested } from 'class-validator';
import { Transform, Type } from 'class-transformer';
import { TransactionTypeDto } from './transaction-type.dto';
import { CustomInputEntity } from './custom-input.entity';

export class TransactionTypeEntity implements TransactionTypeDto {

  @IsString()
  @IsOptional()
  @Transform((_id) => _id && _id.toString())
  _id?: string;

  @IsString()
  name!: string;

  @ValidateNested({ each: true })
  @Type(() => CustomInputEntity)
  @IsOptional()
  customInputs: CustomInputEntity[];

  @IsString()
  @IsOptional()
  explanation?: string;

}

