import { CustomInputDto } from './custom-input.dto';

export interface TransactionTypeDto {

  readonly _id?: string;
  readonly name?: string;
  readonly customInputs?: CustomInputDto[];
  readonly explanation?: string;
}
