import { IsString } from 'class-validator';

export class JwtResponse {

  @IsString()
  access_token: string;

  @IsString()
  refresh_token: string;
}
