import { IsObject } from 'class-validator';

export class DataResponse {

  @IsObject()
  data: any;

  [key: string]: any;
}
