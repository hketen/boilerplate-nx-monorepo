import { IsArray, IsEmail, IsNumber, IsString } from 'class-validator';

export class JwtPayload {

  @IsString()
  userId: string;

  @IsString()
  fullName: string;

  @IsEmail()
  email: string;

  @IsArray()
  roles: string[];

  @IsNumber()
  iat?: number;

  @IsNumber()
  exp?: number;
}
