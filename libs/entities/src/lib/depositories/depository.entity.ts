import { IsOptional, IsString } from 'class-validator';
import { Transform } from 'class-transformer';
import { DepositoryDto } from './depository.dto';

export class DepositoryEntity implements DepositoryDto {

  @IsString()
  @IsOptional()
  @Transform((_id) => _id && _id.toString())
  _id?: string;

  @IsString()
  name: string;

  @IsString()
  @IsOptional()
  explanation?: string;

}

