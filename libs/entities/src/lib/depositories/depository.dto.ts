export interface DepositoryDto {

  readonly _id?: string;
  readonly name?: string;
  readonly explanation?: string;
}
