
# What was used

* Monorepo Nx Workspace: [nx.dev](http://nx.dev) []()
* Backend: NestJS
* Frontend: ReactJS (ant.design)

```
npm i -g nx @nestjs/cli

npm install
```

#### Development
```
... after package install

npm run serve:backend
npm run serve:frontend
```


[Nestjs Documantation](https://docs.nestjs.com/first-steps)
##### Create controller
```
nest generate controller module-name/controllers/controller-name
```
or
```
nest g co module-name/controllers/controllerName
```

##### Create Provider
```
nest g(enerate) s(ervice) module-name/services/service-name
```


#### Build
```
... after package install

npm build 
```



