import React, { useEffect, useState } from 'react';
import { ApiDELETE, ApiGET } from '../services';
import { map } from 'lodash';
import { message } from 'antd';


const useDataList = <T extends unknown>(
  {
    service,
    deleteMessage = 'Silindi!'
  }: { service: string, deleteMessage?: string }
): { data: T[], loading: boolean, getData: Function, onDelete: Function } => {

  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  const getData = async () => {

    setLoading(true);

    try {

      const data = await ApiGET({ service });

      setData(map(data, (item) => ({ key: item._id, ...item })));

    } catch (e) {

      message.error(e.message || 'Unknown Error');
    }

    setLoading(false);
  };

  const onDelete = (id) => async () => {

    setLoading(true);

    try {

      await ApiDELETE({ service: `${service}/${id}` });

      message.success(deleteMessage);

      getData();
    } catch (e) {

      message.error(e.message || 'Unknown Error');
    }

    setLoading(false);
  };

  useEffect(() => {

    getData();

  }, []);

  return {
    data,
    loading,
    getData,
    onDelete
  };
};

export default useDataList;
