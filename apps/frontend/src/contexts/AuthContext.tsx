import React, { useMemo, useState } from 'react';
import { Session } from '../services';
import { Redirect, withRouter } from 'react-router-dom';

export const AuthContext = React.createContext({
  user: null,
  setUser: null
});

export const AuthProvider = withRouter(({ children, location }) => {

  const hasToken = Session.hasToken();
  const { user: initialUser } = Session.get();
  const [user, setUser] = useState(initialUser);

  // Memoized auth context
  const userMemo = useMemo(() => ({
    user,
    setUser,
    setAuth(data) {
      setUser(data);
      Session.set(data);
    }
  }), [user]);

  return (
    <AuthContext.Provider value={userMemo}>
      {
        location.pathname !== '/login' && !hasToken &&
        <Redirect to={{ pathname: '/login', state: { from: location } }}/>
      }
      {children}
    </AuthContext.Provider>
  );
});
