/*
* GeneralProvider creates all the contexts needed by Venuex and wraps with provider
*/
import React, { createContext } from 'react';
import { AuthProvider } from './AuthContext';
import { BrowserRouter as Router } from 'react-router-dom';

export const RootContext = createContext({});

export const RootProvider = ({ children }) => {
  return (
    <RootContext.Provider value={{}}>
      <Router>
        <AuthProvider>
          {children}
        </AuthProvider>
      </Router>
    </RootContext.Provider>
  );
};
