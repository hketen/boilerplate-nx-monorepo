import React, { useEffect, useState } from 'react';
import { Empty, Layout, message } from 'antd';
import { AppPageHeader, Breadcrumbs } from '../../../components';
import { ApiGET, ApiPUT } from '../../../services';
import { AccountEntity } from '@boilerplate-nx-monorepo/entities';
import AccountForm from '../form';
import { useHistory, useParams } from 'react-router-dom';

const { Content } = Layout;

const EditAccountTask = () => {

  const [isLoading, setLoading] = useState<boolean>(false);
  const [isSaveLoading, setSaveLoading] = useState<boolean>(false);
  const [account, setTask] = useState<AccountEntity>();
  const { id } = useParams();
  const history = useHistory();

  const getTask = async () => {

    setLoading(true);
    try {

      const data = await ApiGET({ service: `/accounts/${id}` });

      setTask(data);
    } catch (e) {

      message.error(e.message || 'Bilinmeyen Hata!');
    }

    setLoading(false);
  };

  useEffect(() => {

    getTask();

  }, []);

  const onSubmit = async (values: AccountEntity): Promise<void> => {

    setSaveLoading(true);

    try {

      await ApiPUT({
        service: `/accounts/${id}`,
        payload: { ...values }
      });

      message.success('Save Success');

      history.goBack();
    } catch (e) {
      message.error(e.message || 'Unknown Error');

      setSaveLoading(false);
    }
  };

  return (
    <>
      <AppPageHeader
        title={`${account?.title ?? 'Yükleniyor...'}`}
        breadcrumb={[
          Breadcrumbs.ACCOUNTS,
          Breadcrumbs.ACCOUNT_EDIT(account && account._id)
        ]}
      />

      <Content className="page-content">

        {
          isLoading && <Empty
            description={'Please wait. Loading...'}
          />
        }

        {
          account &&
          <AccountForm loading={isSaveLoading} data={account} onSubmit={onSubmit}/>
        }

      </Content>
    </>
  );
};

export default EditAccountTask;
