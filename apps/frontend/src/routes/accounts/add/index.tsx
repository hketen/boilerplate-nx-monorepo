import React, { useState } from 'react';
import { Layout, message } from 'antd';
import { useHistory } from 'react-router-dom';
import { AppPageHeader, Breadcrumbs } from '../../../components';
import { AccountEntity } from '@boilerplate-nx-monorepo/entities';
import AccountForm from '../form';
import { ApiPOST } from '../../../services';

const { Content } = Layout;

const routes = [
  Breadcrumbs.ACCOUNTS,
  Breadcrumbs.ACCOUNT_ADD
];

const AddAccountTask = () => {

  const history = useHistory();
  const [isLoading, setLoading] = useState(false);

  const onSubmit = async (values: AccountEntity): Promise<void> => {

    setLoading(true);

    try {

      await ApiPOST({
        service: '/accounts',
        payload: { ...values }
      });

      message.success('Kaydedildi!');
      setTimeout(history.goBack);
    } catch (e) {
      message.error(e.message || 'Bilinmeyen Hata!');

      setLoading(false);
    }
  };

  return (
    <>
      <AppPageHeader
        title="Yeni Cari Kart"
        breadcrumb={routes}
      />

      <Content className="page-content">

        <AccountForm loading={isLoading} onSubmit={onSubmit}/>
      </Content>
    </>
  );
};

export default AddAccountTask;
