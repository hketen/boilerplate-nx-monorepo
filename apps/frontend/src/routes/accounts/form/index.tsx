import React, { FC } from 'react';
import { Button, Card, Col, Form, Input, Row } from 'antd';
import { AccountEntity } from '@boilerplate-nx-monorepo/entities';

interface AccountFormProps {

  loading: boolean;
  data?: AccountEntity;
  onSubmit: (payload: AccountEntity) => void | Promise<void>;
}

const AccountForm: FC<AccountFormProps> = ({ data = {}, onSubmit }) => {

  const [form] = Form.useForm();
  // const [selectors, setSelectors] = useState(chain(data).get('selectors').keys().value());

  const onFinish = async (payload: AccountEntity) => {

    onSubmit && await onSubmit({
      ...payload
    });
  };

  const onResetSelectors = () => {
    //
  };

  return (
    <Form
      form={form}
      layout='vertical'
      className="post-form"
      initialValues={data}
      onFinish={onFinish}
    >
      <Row justify="space-between" gutter={12}>
        <Col span={18}>
          <Card bordered={false}>
            <Form.Item
              label="Cari Ünvan"
              name="title"
              rules={[{ required: true, message: 'Cari ünvan zorunludur' }]}
            >
              <Input/>
            </Form.Item>

            <Row gutter={10}>
              <Col span={12}>
                <Form.Item
                  label="TC NO"
                  name="identificationNumber"
                  rules={[{ message: 'TC Numarası 11 Karakter olmalı', len: 11 }]}
                  style={{ width: '100%' }}
                >
                  <Input/>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Input.Group compact>
                  <Form.Item
                    label="Vergi Dairesi"
                    name="taxOffice"
                    style={{ width: '60%' }}
                  >
                    <Input/>
                  </Form.Item>
                  <Form.Item
                    label="Vergi NO"
                    name="taxNumber"
                    style={{ width: '40%' }}
                  >
                    <Input/>
                  </Form.Item>
                </Input.Group>
              </Col>
            </Row>

            <Row gutter={10}>
              <Col span={16}>
                <Form.Item
                  label="Adres"
                  name="address"
                >
                  <Input/>
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item
                  label="Telefon"
                  name="phoneNumber"
                >
                  <Input/>
                </Form.Item>
              </Col>
            </Row>

            <Form.Item
              label="Açıklama"
              name="explanation"
            >
              <Input/>
            </Form.Item>

          </Card>
        </Col>

          <Col span={6}>
          <Card bordered={false}>


            <Form.Item>
              <Button type="primary" htmlType="submit" className="save-button">
                Kaydet
              </Button>
            </Form.Item>
          </Card>
        </Col>
      </Row>
    </Form>
  );
};

export default AccountForm;
