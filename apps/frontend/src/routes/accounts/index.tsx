import React from 'react';
import { Link } from 'react-router-dom';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Layout, Popconfirm, Typography } from 'antd';
import { AppPageHeader, Breadcrumbs, SearchableTable, SearchableTableColumn } from '../../components';
import { AccountEntity } from '@boilerplate-nx-monorepo/entities';
import { useDataList } from '../../hooks';

const { Text } = Typography;

const routes = [
  Breadcrumbs.ACCOUNTS
];

const Accounts = () => {

  const { data: accounts, loading, onDelete } = useDataList({
    service: '/accounts',
    deleteMessage: 'Cari kart silindi!'
  });

  const onFilterString = ({ title, phoneNumber, address, explanation, taxNumber, identificationNumber, taxOffice }: AccountEntity) => {

    return `${title} ${phoneNumber} ${address} ${explanation} ${taxNumber} ${identificationNumber} ${taxOffice}`;
  };

  return <>
    <AppPageHeader
      breadcrumb={routes}
    />

    <Layout.Content className="page-content accounts">

      <SearchableTable
        title="Cari Kartlar"
        dataSource={accounts}
        onFilterString={onFilterString}
        loading={loading}
        extra={
          <Link to="/accounts/add">
            <Button
              type="primary"
              ghost
              size="small"
              icon={<PlusOutlined/>}
            />
          </Link>
        }>
        <SearchableTableColumn
          title="Cari Ünvanı"
          dataIndex="title"
          key="title"
          render={(title) => (
            <Text>{title}</Text>
          )}
        />
        <SearchableTableColumn
          title="Kimlik Numarası"
          dataIndex="identificationNumber"
          key="identificationNumber"
          render={(identificationNumber) => (
            <Text>{identificationNumber}</Text>
          )}
        />
        <SearchableTableColumn
          title="Telefon Numarası"
          dataIndex="phoneNumber"
          key="phoneNumber"
          render={(identificationNumber) => (
            <Text>{identificationNumber}</Text>
          )}
        />
        <SearchableTableColumn
          title="Vergi Dairesi"
          dataIndex="taxOffice"
          key="taxOffice"
          render={(taxOffice) => (
            <Text>{taxOffice}</Text>
          )}
        />
        <SearchableTableColumn
          title="Vergi No"
          dataIndex="taxNumber"
          key="taxNumber"
          render={(taxNumber) => (
            <Text>{taxNumber}</Text>
          )}
        />
        <SearchableTableColumn
          title="Actions"
          dataIndex="_id"
          key="_id"
          width={90}
          align="center"
          render={(id) => (
            <div className="actions">
              <Link to={`/accounts/${id}/edit`}>
                <Button
                  type="primary"
                  ghost
                  size="small"
                  icon={<EditOutlined/>}
                />
              </Link>

              <Popconfirm
                placement="bottomRight"
                title="Cari kart silinsin mi?"
                onConfirm={onDelete(id)}
                okText="Yes"
                cancelText="No"
              >
                <Button danger ghost size="small" icon={<DeleteOutlined/>}/>
              </Popconfirm>
            </div>
          )}
        />
      </SearchableTable>
    </Layout.Content>
  </>;
};

export default Accounts;
