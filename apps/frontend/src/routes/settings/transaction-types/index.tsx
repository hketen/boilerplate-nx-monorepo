import React from 'react';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Layout, Popconfirm, Typography } from 'antd';
import { Link } from 'react-router-dom';
import { AppPageHeader, Breadcrumbs, SearchableTable, SearchableTableColumn } from '../../../components';
import { useDataList } from '../../../hooks';

const { Text } = Typography;

const TransactionTypes = () => {

  const { data: transactionTypes, loading, onDelete } = useDataList({
    service: '/transaction-types',
    deleteMessage: 'İşlem Tipi Silindi!'
  });

  const onFilterString = ({ name }) => `${name}`;

  return <>
    <AppPageHeader
      breadcrumb={[
        Breadcrumbs.TRANSACTION_TYPES
      ]}
    />

    <Layout.Content className="page-content">
      <SearchableTable
        title="İşlem Tipleri"
        dataSource={transactionTypes}
        onFilterString={onFilterString}
        loading={loading}
        extra={
          <>
            <Link to="/settings/transaction-types/add">
              <Button
                type="primary"
                ghost
                size="small"
                icon={<PlusOutlined/>}
              />
            </Link>
          </>
        }>
        <SearchableTableColumn
          title="İşlem Tipi Adı"
          dataIndex="name"
          key="name"
          render={(name) => (
            <Text>{name}</Text>
          )}
        />
        <SearchableTableColumn
          title="Açıklama"
          dataIndex="explanation"
          key="explanation"
          render={(explanation) => (
            <Text>{explanation}</Text>
          )}
        />
        <SearchableTableColumn
          title="Actions"
          dataIndex="_id"
          key="_id"
          width={90}
          align="center"
          render={(id) => (
            <div className="actions">
              <Link to={`/settings/transaction-types/${id}/edit`}>
                <Button
                  type="primary"
                  ghost
                  size="small"
                  icon={<EditOutlined/>}
                />
              </Link>

              <Popconfirm
                placement="bottomRight"
                title="İşlem Tipi silinsin mi?"
                onConfirm={onDelete(id)}
                okText="Yes"
                cancelText="No"
              >
                <Button danger ghost size="small" icon={<DeleteOutlined/>}/>
              </Popconfirm>
            </div>
          )}
        />
      </SearchableTable>
    </Layout.Content>
  </>;
};

export default TransactionTypes;
