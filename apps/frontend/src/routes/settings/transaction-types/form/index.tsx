import React, { FC, useContext } from 'react';
import { Button, Card, Col, Form, Input, Row, Space } from 'antd';
import { TransactionTypeEntity } from '@boilerplate-nx-monorepo/entities';
import { AuthContext } from '../../../../contexts/AuthContext';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons/lib';
import { isWithRoles } from '@boilerplate-nx-monorepo/common-utils';
import { InputTypeSelect } from '../../../../components';

interface TransactionTypeProps {

  loading: boolean;
  data?: TransactionTypeEntity;
  onSubmit: (payload: TransactionTypeEntity) => void | Promise<void>;
}

const TransactionTypeForm: FC<TransactionTypeProps> = ({ data = {}, onSubmit }) => {

  const { user: { roles } } = useContext(AuthContext);
  const [form] = Form.useForm();
  const disabled = !isWithRoles(['root'], roles);
  const onFinish = async (payload: TransactionTypeEntity) => {

    onSubmit && await onSubmit({
      ...payload
    });
  };

  return (
    <Form
      form={form}
      layout='vertical'
      className="post-form"
      initialValues={data}
      onFinish={onFinish}
    >
      <Row justify="space-between" gutter={12}>
        <Col span={18}>
          <Card bordered={false}>
            <Form.Item
              label="İşlem Tipi adı"
              name="name"
              rules={[{ required: true, message: 'İşlem Tipi adı zorunludur' }]}
            >
              <Input/>
            </Form.Item>

            <Form.Item
              label="Açıklama"
              name="explanation"
            >
              <Input/>
            </Form.Item>

            {disabled && <>Özel Alanları sadece yönetici değiştirebilir!</>}
            <Form.List name="customInputs">
              {(fields, { add, remove }) => {
                return (
                  <div>
                    {fields.map(field => (
                      <Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="start">
                        <Form.Item
                          {...field}
                          name={[field.name, 'inputType']}
                          fieldKey={[field.fieldKey, 'inputType']}
                          rules={[{ required: true, message: 'Tip Seçiniz' }]}
                        >
                          <InputTypeSelect disabled={disabled}/>
                        </Form.Item>
                        <Form.Item
                          {...field}
                          name={[field.name, 'inputKey']}
                          fieldKey={[field.fieldKey, 'inputKey']}
                          rules={[{ required: true, message: 'Anahtar zorunlu alandır!' }]}
                        >
                          <Input placeholder="Anahtar" disabled={disabled}/>
                        </Form.Item>
                        <Form.Item
                          {...field}
                          name={[field.name, 'label']}
                          fieldKey={[field.fieldKey, 'label']}
                          rules={[{ required: true, message: 'Etiket zorunlu alandır!' }]}
                        >
                          <Input placeholder="Etiket" disabled={disabled}/>
                        </Form.Item>

                        {
                          !disabled &&
                          <Button
                            type="ghost"
                            onClick={() => remove(field.name)}
                          >
                            <MinusCircleOutlined/>
                          </Button>
                        }
                      </Space>
                    ))}

                    <Form.Item>
                      <Button
                        disabled={disabled}
                        type="dashed"
                        onClick={() => {
                          add();
                        }}
                        block
                      >
                        <PlusOutlined/> Özel Alan Ekle
                      </Button>
                    </Form.Item>
                  </div>
                );
              }}
            </Form.List>
          </Card>
        </Col>

        <Col span={6}>
          <Card bordered={false}>


            <Form.Item>
              <Button type="primary" htmlType="submit" className="save-button">
                Kaydet
              </Button>
            </Form.Item>
          </Card>
        </Col>
      </Row>
    </Form>
  );
};

export default TransactionTypeForm;
