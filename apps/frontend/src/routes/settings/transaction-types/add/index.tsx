import React, { useState } from 'react';
import { Layout, message } from 'antd';
import { useHistory } from 'react-router-dom';
import { AppPageHeader, Breadcrumbs } from '../../../../components';
import { TransactionTypeEntity } from '@boilerplate-nx-monorepo/entities';
import DepositoryForm from '../form';
import { ApiPOST } from '../../../../services';

const { Content } = Layout;

const AddTransactionType = () => {

  const history = useHistory();
  const [isLoading, setLoading] = useState(false);

  const onSubmit = async (values: TransactionTypeEntity): Promise<void> => {

    setLoading(true);

    try {

      await ApiPOST({
        service: '/transaction-types',
        payload: { ...values }
      });

      message.success('Kaydedildi!');
      setTimeout(history.goBack);
    } catch (e) {
      message.error(e.message || 'Bilinmeyen Hata!');

      setLoading(false);
    }
  };

  return (
    <>
      <AppPageHeader
        title="Yeni İşlem Tipi"
        breadcrumb={[
          Breadcrumbs.TRANSACTION_TYPES,
          Breadcrumbs.TRANSACTION_TYPE_ADD
        ]}
      />

      <Content className="page-content">

        <DepositoryForm loading={isLoading} onSubmit={onSubmit}/>
      </Content>
    </>
  );
};

export default AddTransactionType;
