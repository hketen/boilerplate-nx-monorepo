import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { Empty, Layout, message } from 'antd';
import { AppPageHeader, Breadcrumbs } from '../../../../components';
import { ApiGET, ApiPUT } from '../../../../services';
import { TransactionTypeEntity } from '@boilerplate-nx-monorepo/entities';
import TransactionTypeForm from '../form';

const { Content } = Layout;

const EditTransactionType = () => {

  const [isLoading, setLoading] = useState<boolean>(false);
  const [isSaveLoading, setSaveLoading] = useState<boolean>(false);
  const [transactionType, setTransactionTypes] = useState<TransactionTypeEntity>();
  const { id } = useParams();
  const history = useHistory();

  const getTransactionTypes = async () => {

    setLoading(true);
    try {

      const data = await ApiGET({ service: `/transaction-types/${id}` });

      setTransactionTypes(data);
    } catch (e) {

      message.error(e.message || 'Bilinmeyen Hata!');
    }

    setLoading(false);
  };

  useEffect(() => {

    getTransactionTypes();

  }, []);

  const onSubmit = async (values: TransactionTypeEntity): Promise<void> => {

    setSaveLoading(true);

    try {

      await ApiPUT({
        service: `/transaction-types/${id}`,
        payload: { ...values }
      });

      message.success('Save Success');

      history.goBack();
    } catch (e) {
      message.error(e.message || 'Unknown Error');

      setSaveLoading(false);
    }
  };

  return (
    <>
      <AppPageHeader
        title={`${transactionType?.name ?? 'Yükleniyor...'}`}
        breadcrumb={[
          Breadcrumbs.TRANSACTION_TYPES,
          Breadcrumbs.TRANSACTION_TYPE_EDIT(transactionType && transactionType._id)
        ]}
      />

      <Content className="page-content">

        {
          isLoading && <Empty
            description={'Please wait. Loading...'}
          />
        }

        {
          transactionType &&
          <TransactionTypeForm loading={isSaveLoading} data={transactionType} onSubmit={onSubmit}/>
        }

      </Content>
    </>
  );
};

export default EditTransactionType;
