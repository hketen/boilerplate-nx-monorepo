import React, { useState } from 'react';
import { Layout, message } from 'antd';
import { useHistory } from 'react-router-dom';
import { AppPageHeader, Breadcrumbs } from '../../../../components';
import { DepositoryEntity } from '@boilerplate-nx-monorepo/entities';
import DepositoryForm from '../form';
import { ApiPOST } from '../../../../services';

const { Content } = Layout;

const AddDepository = () => {

  const history = useHistory();
  const [isLoading, setLoading] = useState(false);

  const onSubmit = async (values: DepositoryEntity): Promise<void> => {

    setLoading(true);

    try {

      await ApiPOST({
        service: '/depositories',
        payload: { ...values }
      });

      message.success('Kaydedildi!');
      setTimeout(history.goBack);
    } catch (e) {
      message.error(e.message || 'Bilinmeyen Hata!');

      setLoading(false);
    }
  };

  return (
    <>
      <AppPageHeader
        title="Yeni İşlem Noktası"
        breadcrumb={[
          Breadcrumbs.DEPOSITORIES,
          Breadcrumbs.DEPOSITORY_ADD
        ]}
      />

      <Content className="page-content">

        <DepositoryForm loading={isLoading} onSubmit={onSubmit}/>
      </Content>
    </>
  );
};

export default AddDepository;
