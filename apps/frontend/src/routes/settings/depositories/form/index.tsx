import React, { FC } from 'react';
import { Button, Card, Col, Form, Input, Row } from 'antd';
import { DepositoryEntity } from '@boilerplate-nx-monorepo/entities';

interface DepositoryFormProps {

  loading: boolean;
  data?: DepositoryEntity;
  onSubmit: (payload: DepositoryEntity) => void | Promise<void>;
}

const DepositoryForm: FC<DepositoryFormProps> = ({ data = {}, onSubmit }) => {

  const [form] = Form.useForm();
  // const [selectors, setSelectors] = useState(chain(data).get('selectors').keys().value());

  const onFinish = async (payload: DepositoryEntity) => {

    onSubmit && await onSubmit({
      ...payload
    });
  };

  const onResetSelectors = () => {
    //
  };

  return (
    <Form
      form={form}
      layout='vertical'
      initialValues={data}
      onFinish={onFinish}
    >
      <Row justify="space-between" gutter={12}>
        <Col span={18}>
          <Card bordered={false}>
            <Form.Item
              label="İşlem Noktası adı"
              name="name"
              rules={[{ required: true, message: 'İşlem Noktası adı zorunludur' }]}
            >
              <Input/>
            </Form.Item>

            <Form.Item
              label="Açıklama"
              name="explanation"
            >
              <Input/>
            </Form.Item>

          </Card>
        </Col>

        <Col span={6}>
          <Card bordered={false}>


            <Form.Item>
              <Button type="primary" htmlType="submit" className="save-button">
                Kaydet
              </Button>
            </Form.Item>
          </Card>
        </Col>
      </Row>
    </Form>
  );
};

export default DepositoryForm;
