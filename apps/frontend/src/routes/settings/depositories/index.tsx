import React from 'react';
import { Link } from 'react-router-dom';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Layout, Popconfirm, Typography } from 'antd';
import { AppPageHeader, Breadcrumbs, SearchableTable, SearchableTableColumn } from '../../../components';
import { useDataList } from '../../../hooks';

const { Text } = Typography;

const Depositories = () => {

  const { data: depositories, loading, onDelete } = useDataList({
    service: '/depositories',
    deleteMessage: 'İşlem Noktası Silindi!'
  });

  const onFilterString = ({ name }) => `${name}`;

  return <>
    <AppPageHeader
      breadcrumb={[
        Breadcrumbs.DEPOSITORIES
      ]}
    />

    <Layout.Content className="page-content">
      <SearchableTable
        title="İşlem Noktası"
        dataSource={depositories}
        onFilterString={onFilterString}
        loading={loading}
        extra={
          <>
            <Link to="/settings/depositories/add">
              <Button
                type="primary"
                ghost
                size="small"
                icon={<PlusOutlined/>}
              />
            </Link>
          </>
        }>
        <SearchableTableColumn
          title="İşlem Noktası Adı"
          dataIndex="name"
          key="name"
          render={(name) => (
            <Text>{name}</Text>
          )}
        />
        <SearchableTableColumn
          title="Açıklama"
          dataIndex="explanation"
          key="explanation"
          render={(explanation) => (
            <Text>{explanation}</Text>
          )}
        />
        <SearchableTableColumn
          title="Actions"
          dataIndex="_id"
          key="_id"
          width={90}
          align="center"
          render={(id) => (
            <div className="actions">
              <Link to={`/settings/depositories/${id}/edit`}>
                <Button
                  type="primary"
                  ghost
                  size="small"
                  icon={<EditOutlined/>}
                />
              </Link>

              <Popconfirm
                placement="bottomRight"
                title="İşlem Noktası silinsin mi?"
                onConfirm={onDelete(id)}
                okText="Yes"
                cancelText="No"
              >
                <Button danger ghost size="small" icon={<DeleteOutlined/>}/>
              </Popconfirm>
            </div>
          )}
        />
      </SearchableTable>
    </Layout.Content>
  </>;
};

export default Depositories;
