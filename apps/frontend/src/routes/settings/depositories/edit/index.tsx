import React, { useEffect, useState } from 'react';
import { Empty, Layout, message } from 'antd';
import { AppPageHeader, Breadcrumbs } from '../../../../components';
import { ApiGET, ApiPUT } from '../../../../services';
import { DepositoryEntity } from '@boilerplate-nx-monorepo/entities';
import DepositoryForm from '../form';
import { useHistory, useParams } from 'react-router-dom';

const { Content } = Layout;

const EditDepository = () => {

  const [isLoading, setLoading] = useState<boolean>(false);
  const [isSaveLoading, setSaveLoading] = useState<boolean>(false);
  const [depository, setTask] = useState<DepositoryEntity>();
  const { id } = useParams();
  const history = useHistory();

  const getTask = async () => {

    setLoading(true);
    try {

      const data = await ApiGET({ service: `/depositories/${id}` });

      setTask(data);
    } catch (e) {

      message.error(e.message || 'Bilinmeyen Hata!');
    }

    setLoading(false);
  };

  useEffect(() => {

    getTask();

  }, []);

  const onSubmit = async (values: DepositoryEntity): Promise<void> => {

    setSaveLoading(true);

    try {

      await ApiPUT({
        service: `/depositories/${id}`,
        payload: { ...values }
      });

      message.success('Save Success');

      history.goBack();
    } catch (e) {
      message.error(e.message || 'Unknown Error');

      setSaveLoading(false);
    }
  };

  return (
    <>
      <AppPageHeader
        title={`${depository?.name ?? 'Yükleniyor...'}`}
        breadcrumb={[
          Breadcrumbs.DEPOSITORIES,
          Breadcrumbs.DEPOSITORY_EDIT(depository && depository._id)
        ]}
      />

      <Content className="page-content">

        {
          isLoading && <Empty
            description={'Please wait. Loading...'}
          />
        }

        {
          depository &&
          <DepositoryForm loading={isSaveLoading} data={depository} onSubmit={onSubmit}/>
        }

      </Content>
    </>
  );
};

export default EditDepository;
