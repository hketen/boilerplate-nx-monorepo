import LoginView from './login';
import HomeView from './home';
import AccountsView from './accounts';
import AccountAddView from './accounts/add';
import AccountEditView from './accounts/edit';
import DepositoriesView from './settings/depositories';
import DepositoryAddView from './settings/depositories/add';
import DepositoryEditView from './settings/depositories/edit';
import TransactionTypesView from './settings/transaction-types';
import TransactionTypeAddView from './settings/transaction-types/add';
import TransactionTypeEditView from './settings/transaction-types/edit';
import ProfileView from './profile';
import Error404View from './error404';

export {
  LoginView,

  HomeView,

  AccountsView,
  AccountAddView,
  AccountEditView,

  DepositoriesView,
  DepositoryAddView,
  DepositoryEditView,

  TransactionTypesView,
  TransactionTypeAddView,
  TransactionTypeEditView,

  ProfileView,

  Error404View
};
