import React from 'react';
import { Empty, Layout } from 'antd';
import { AppPageHeader } from '../../components';
import './style.scss';

const Home = () => {

  return <>
    <AppPageHeader/>

    <Layout.Content className="page-content">

      <Empty description={false}/>
    </Layout.Content>
  </>;
};

export default Home;
