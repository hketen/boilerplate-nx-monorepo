import React from 'react';
import './style.scss';
import { Button, Card, Layout } from 'antd';
import AppPageHeader from '../../components/AppPageHeader';
import { selfRedirect, Session } from '../../services';

const { Content } = Layout;

const routes = [
  {
    path: '/profile',
    text: 'Profile'
  }
];

const Profile = () => {

  const logout = async () => {

    Session.clear();

    selfRedirect('/login');
  };

  return (
    <>
      <AppPageHeader
        breadcrumb={routes}
        title="Profile"
        buttons={
          <>
            <Button danger ghost onClick={logout}>
              Logout
            </Button>
          </>
        }
      />

      <Content className="page-content content">
        <Card bordered={false}>

          şifre ve rol değiştirme işlemleri buraya hazırlanacak.

        </Card>
      </Content>
    </>
  );
};

export default Profile;
