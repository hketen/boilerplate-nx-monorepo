import React from 'react';
import { LeftOutlined } from '@ant-design/icons';
import { Button, Card, Layout } from 'antd';
import { withRouter } from 'react-router-dom';
import './style.scss';

const Error404 = ({ history }) => {

  return (
    <Layout.Content className="error404-page">


      <Card bordered={false}>

        404 Kere Aradık Tarafık Hiç Bişey Bulamadık :(

      </Card>

      <Button type="link" block onClick={history.goBack}>
        <LeftOutlined />
        Back
      </Button>

    </Layout.Content>
  );
};

export default withRouter(Error404);
