import React, { FC, useState } from 'react';
import { Button, Card, Form, Input, Layout } from 'antd';
import { LockOutlined, MailOutlined } from '@ant-design/icons/lib';
import { ApiPOST, selfRedirect, Session } from '../../services';
import './style.scss';

const Login: FC = () => {

  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);

  const onFinish = async (payload) => {

    setLoading(true);

    try {

      const data = await ApiPOST({
        service: '/auth/login',
        payload,
        withoutAuth: true
      });

      if (data) {
        Session.set(data);

        selfRedirect('/');
      }
    } catch (e) {

      // skip error.
    }

    setLoading(false);
  };

  return (
    <Layout.Content className="page-content login">
      <Card title="Giriş Yapın" bordered={false}>
        <Form
          form={form}
          onFinish={onFinish}
          className="login-form"
        >
          <Form.Item
            name="email"
            rules={[{ required: true, message: 'Please input your email!' }]}
          >
            <Input
              prefix={<MailOutlined/>}
              placeholder="Email"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: 'Please input your Password!' }]}
          >
            <Input
              prefix={<LockOutlined/>}
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button" loading={loading}>
              Log in
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </Layout.Content>
  );
};

export default Login;
