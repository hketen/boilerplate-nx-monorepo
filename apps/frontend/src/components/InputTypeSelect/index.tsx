import React from 'react';
import { map } from 'lodash';
import { Button, Select } from 'antd';
import { environment as env } from '../../environments/environment';

const { CustomInputTypes } = env;

const InputTypeSelect = (props) => (
  <Select
    {...props}
    placeholder="Tip Seçin"
    style={{ minWidth: '230px' }}
  >
    {
      map(CustomInputTypes, ({ value, label }) => (
        <Select.Option key={`${value}`} value={value}>
          {label}
        </Select.Option>
      ))
    }
  </Select>
);

export default InputTypeSelect;
