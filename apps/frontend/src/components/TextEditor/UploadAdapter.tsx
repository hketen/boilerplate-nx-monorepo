import * as _ from 'lodash';
import { ApiPOST } from '../../services';

export class UploadAdapter {

  loader = null;

  constructor(loader) {
    // CKEditor 5's FileLoader instance.
    this.loader = loader;
  }

  // Starts the upload process.
  async upload() {
    try {
      const file = await this.loader.file;

      const payload = new FormData();
      payload.append('files', file);

      const data = await ApiPOST(
        {
          service: '/upload',
          payload,
          isUpload: true,
          options: {
            onUploadProgress: (evt) => {

              if (evt.lengthComputable) {
                this.loader.uploadTotal = evt.total;
                this.loader.uploaded = evt.loaded;
              }
            }
          }
        }
      );

      return {
        default: _.result(data, 'uploaded[0].filename')
      };

    } catch (e) {

      return {
        'uploaded': 0,
        'error': {
          'message': 'The file is too big.'
        }
      };
    }
  }

  // Aborts the upload process.
  abort() {
    console.log('loader:abort');
  }
}
