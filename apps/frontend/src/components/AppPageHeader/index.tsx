import React, { FC, ReactNode } from 'react';
import { Breadcrumb } from 'antd';
import { Link } from 'react-router-dom';
import { map } from 'lodash';
import { Breadcrumbs } from './Breadcrumbs';

import './style.scss';

interface AppPageHeaderProps {
  title?: string,
  subtitle?: string,
  breadcrumb?: { text: string, icon?: string, path: string }[],
  tags?: { text: string, color: string }[],
  buttons?: ReactNode
}

const AppPageHeader: FC<AppPageHeaderProps> = ({ title, subtitle, breadcrumb, tags, buttons }) => {

  return (
    <div className="ant-page-header has-breadcrumb">
      {
        breadcrumb &&
        <Breadcrumb>
          {
            map([Breadcrumbs.HOME, ...breadcrumb], (route, index) => (
              <Breadcrumb.Item key={route.path}>
                <Link to={route.path}>{route.icon}{route.icon && ' '}{route.text}</Link>
              </Breadcrumb.Item>
            ))
          }
        </Breadcrumb>
      }
      <div className="ant-page-header-heading">
        {
          title &&
          <span className="ant-page-header-heading-title">{title}</span>
        }
        {
          subtitle &&
          <span className="ant-page-header-heading-sub-title">{subtitle}</span>
        }

        <span className="ant-page-header-heading-tags">
          {
            map(tags, (tag) => (
              <span className={`ant-tag ant-tag-${tag.color || 'blue'}`}>
                  {tag.text}
              </span>
            ))
          }
        </span>
        <span className="ant-page-header-heading-extra">
          {buttons}
        </span>
      </div>
    </div>
  );
};

export default AppPageHeader;
