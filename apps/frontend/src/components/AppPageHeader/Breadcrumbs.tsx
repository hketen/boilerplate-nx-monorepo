import React from 'react';
import { HomeOutlined } from '@ant-design/icons';

/**
 *
 */
export class Breadcrumbs {
  static HOME = {
    path: '/',
    text: 'Dashboard',
    icon: <HomeOutlined/>
  };

  static ACCOUNTS = {
    path: '/accounts',
    text: 'Cari Kartlar'
  };

  static ACCOUNT_ADD = {
    path: '/accounts/add',
    text: 'Yeni Cari Kart'
  };

  static ACCOUNT_EDIT = (accountId) => ({
    path: `/accounts/${accountId}/edit`,
    text: 'Cari kart Düzenle'
  });

  static TRANSACTIONS = {
    path: '/transactions',
    text: 'İşlemler'
  };

  static TRANSACTION_ADD = {
    path: '/transactions/add',
    text: 'Yeni İşlem'
  };

  static TRANSACTION_EDIT = (transactionId) => ({
    path: `/transactions/${transactionId}/edit`,
    text: 'İşlem Kartı Düzenle'
  });

  static TRANSACTION_DETAIL_ADD = (transactionId) => ({
    path: `/transactions/${transactionId}/details/add`,
    text: 'İşlem Detay Ekle'
  });

  static TRANSACTION_DETAIL_EDIT = (transactionId, detailId) => ({
    path: `/transactions/${transactionId}/details/${detailId}/edit`,
    text: 'İşlem Detay Ekle'
  });

  static DEPOSITORIES = {
    path: '/settings/depositories',
    text: 'İşlem Noktası'
  };

  static DEPOSITORY_ADD = {
    path: '/settings/depositories/add',
    text: 'Yeni İşlem Noktası'
  };

  static DEPOSITORY_EDIT = (depositoryId) => ({
    path: `/settings/depositories/${depositoryId}/edit`,
    text: 'İşlem Noktası Düzenle'
  });

  static TRANSACTION_TYPES = {
    path: '/settings/transaction-types',
    text: 'İşlem Tipleri'
  };

  static TRANSACTION_TYPE_ADD = {
    path: '/settings/transaction-types/add',
    text: 'Yeni İşlem Tipi'
  };

  static TRANSACTION_TYPE_EDIT = (transactionTypeId) => ({
    path: `/settings/transaction-types/${transactionTypeId}/edit`,
    text: "İşlem Tipi Düzenle"
  });

}
