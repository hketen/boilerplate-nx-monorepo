import React, { FC } from 'react';
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom';

const LoginRedirect: FC<RouteComponentProps> = ({ location }) => {

  return (<Redirect to={{ pathname: '/login', state: { from: location } }}/>);
};

export default withRouter(LoginRedirect);
