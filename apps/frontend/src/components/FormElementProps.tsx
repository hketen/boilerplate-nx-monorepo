export interface FormElementProps<T> {

  value?: T;
  onChange?: (value: T) => void;

  [key: string]: any;
}
