import React, { FC, useEffect, useState } from 'react';
import { filter, indexOf, map } from 'lodash';
import { PlusOutlined } from '@ant-design/icons';
import { Input, Tag, Tooltip } from 'antd';
import { FormElementProps } from '../FormElementProps';
import './style.scss';

export interface TagItemProps extends FormElementProps<string[]> {
  closable?: boolean,
}

const TagItems: FC<TagItemProps> = ({ value, onChange, closable }) => {

  const [inputRef, setInputRef] = useState(null);
  const [inputVisible, setInputVisible] = useState(false);
  const [inputValue, setInputValue] = useState('');

  const handleClose = removedTag => {
    const tags = filter(value, tag => tag !== removedTag);
    onChange(tags);
  };

  const showInput = () => {

    setInputVisible(true);
  };

  const handleInputChange = ({ target }) => {
    setInputValue(target.value);
  };

  const handleInputConfirm = () => {

    if (inputValue && indexOf(value, inputValue) === -1) {
      setInputVisible(false);
      setInputValue('');
      onChange([...value, inputValue]);
    }
  };

  useEffect(() => {

    if (inputRef) {
      inputRef.focus();
    }

  }, [inputRef]);

  return (
    <div>
      {
        map(value, (tag) => {
          const isLongTag = tag.length > 20;
          const tagElem = (
            <Tag key={tag} closable={closable} onClose={() => handleClose(tag)}>
              {isLongTag ? `${tag.slice(0, 20)}...` : tag}
            </Tag>
          );
          return isLongTag
            ? (
              <Tooltip title={tag} key={tag}>{tagElem}</Tooltip>
            )
            : (tagElem);
        })
      }
      {inputVisible && (
        <Input
          ref={setInputRef}
          type="text"
          size="small"
          style={{ width: 78 }}
          value={inputValue}
          onChange={handleInputChange}
          onBlur={handleInputConfirm}
          onPressEnter={handleInputConfirm}
        />
      )}
      {!inputVisible && (
        <Tag onClick={showInput} style={{ background: '#fff', borderStyle: 'dashed' }}>
          <PlusOutlined /> New Tag
        </Tag>
      )}
    </div>
  );
};

export default TagItems;
