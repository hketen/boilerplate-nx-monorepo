import React, { FC, useContext } from 'react';
import { Redirect, Route, RouteComponentProps, withRouter } from 'react-router-dom';
// Local Imports
import { AuthContext } from '../../contexts/AuthContext';

interface PrivateRouteProps extends RouteComponentProps {
  component: FC<RouteComponentProps>,
}

const PrivateRoute: FC<PrivateRouteProps> = ({ component: Component, location, ...rest }) => {

  const { user } = useContext(AuthContext);
  const { pathname, search } = location;

  if (!user) {
    return (
      <>
        <Redirect
          to={{ pathname: '/account/login', search: `?redirect_uri=${encodeURIComponent(`${pathname}${search}`)}` }}/>
      </>
    );
  }

  return (
    <Route
      {...rest}
      render={(props) => (
        <Component {...props}/>
      )}
    />
  );
};

export default withRouter(PrivateRoute);
