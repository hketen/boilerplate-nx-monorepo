import React from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;


const AppFooter = () => {

  return (
    <Footer
      style={{ textAlign: 'center' }}
    >
      WebX Design ©2018 Created by Hakan KETEN
    </Footer>
  );
};

export default AppFooter;
