import React, { FC } from 'react';
import { Select as BaseSelect } from 'antd';
import { get, map } from 'lodash';
import { FormElementProps } from '../FormElementProps';

interface ApiSelectProps extends FormElementProps<string> {

  options: any[];
  labelField: any;
  valueField?: any;
}

const Select: FC<ApiSelectProps> = ({ options = [], labelField, valueField = '_id', ...otherProps }: ApiSelectProps) => {

  return (
    <BaseSelect
      showSearch
      {...otherProps}
      loading={!options.length}
    >
      {
        map(options, (item) => (
          <BaseSelect.Option
            key={`${get(item, valueField)}`}
            value={`${get(item, valueField)}`}>
            {get(item, labelField)}
          </BaseSelect.Option>
        ))
      }
    </BaseSelect>
  );
};

export default Select;
