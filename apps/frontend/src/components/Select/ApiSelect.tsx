import React, { FC, useEffect, useState } from 'react';
import { message } from 'antd';
import { ApiGET } from '../../services';
import { FormElementProps } from '../FormElementProps';
import Select from './';

interface ApiSelectProps extends FormElementProps<string> {

  apiOpts: any;
  labelField: any;
  valueField?: any;
}

const ApiSelect: FC<ApiSelectProps> = ({ apiOpts, ...otherProps }: ApiSelectProps) => {

  const [options, setOptions] = useState([]);

  const getData = async () => {
    try {

      const _data = await ApiGET(apiOpts);

      setOptions(_data);
    } catch (e) {

      message.error(e.message || 'Unknown Error');
    }
  };

  useEffect(() => {

    getData();
  }, []);

  return (<Select options={options} {...otherProps}/>);
};

export default ApiSelect;
