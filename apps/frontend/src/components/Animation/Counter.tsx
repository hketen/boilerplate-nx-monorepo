import React, { FC } from 'react';
import TweenOne from 'rc-tween-one';
import Children from 'rc-tween-one/lib/plugin/ChildrenPlugin';

TweenOne.plugins.push(Children);

interface CounterProps {

  from?: number;
  to: number;
  floatLength?: number;
  animationProps?: any
}

const Counter: FC<CounterProps> = ({ from, to, animationProps, floatLength = 0 }) => {

  return (
    <TweenOne
      animation={{
        Children: { value: to, floatLength },
        duration: 1000
      }}
      {...animationProps}
    >
      {from || 0}
    </TweenOne>
  );
};

export default Counter;
