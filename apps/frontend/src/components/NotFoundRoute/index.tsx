import React, { FC } from 'react';
import { Redirect, Route, RouteComponentProps, withRouter } from 'react-router-dom';

// Local Imports


const NotFoundRoute: FC<RouteComponentProps> = () => {

  return (
    <Route exact path="**">
      <Redirect to="/404"/>
    </Route>
  );
};

export default withRouter(NotFoundRoute);
