import React, { FC, useEffect, useState } from 'react';
import { Card, Input, Space, Table } from 'antd';
import { filter, isEmpty } from 'lodash';

interface SearchableTableProps<TData = any[]> {

  dataSource: TData,
  onFilterString: (item) => string,
  extra?: any

  [key: string]: any
}

const SearchableTable: FC<SearchableTableProps> = (
  {
    title,
    dataSource,
    children,
    onFilterString,
    extra,
    ...otherProps
  }
) => {

  const [filteredDataSource, setFilteredDataSource] = useState(dataSource || []);
  const [searchValue, setSearchValue] = useState('');

  const onSearch = ({ target: { value: currValue } }) => {

    setSearchValue(currValue);
    const filtered = isEmpty(currValue)
      ? dataSource
      : filter(dataSource, (item) => {

        const texts = currValue.split(' ')
          .filter((t) => !isEmpty(t))
          .map((t) => t.toLocaleLowerCase('tr'));

        const searchText = onFilterString(item)
          .toString()
          .toLocaleLowerCase('tr');

        // ^(?=.*word1)(?=.*word2).*$
        const regex = new RegExp(`^(?=.*${texts.join(')(?=.*')}).*$`);

        return regex.test(searchText);
      });

    setFilteredDataSource(filtered);
  };

  useEffect(() => {

    setFilteredDataSource(dataSource);
  }, [dataSource]);

  return (
    <Card
      bordered={false}
      title={title}
      extra={
        <Space>
          <Input
            placeholder="Ara"
            value={searchValue}
            onChange={onSearch}
          />
          {extra}
        </Space>
      }
    >

      <Table
        dataSource={filteredDataSource}
        size="small"
        bordered={true}
        {...otherProps}
      >
        {children}
      </Table>
    </Card>
  );
};

export default SearchableTable;
export const SearchableTableColumn = Table.Column;
