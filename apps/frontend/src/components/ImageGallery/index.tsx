import React, { forwardRef, useState } from 'react';
import { get, isNil, map } from 'lodash';
import { PlusOutlined } from '@ant-design/icons';
import { Modal, Upload } from 'antd';
import { environment as env } from '../../environments/environment';
import { ImageEntity } from '@boilerplate-nx-monorepo/entities';

import './style.scss';

interface ImageGalleryProps {
  value?: {
    uid: string,
    name: string,
    status: 'done',
    url: string,
  },
  onChange: (images: ImageEntity[]) => void,
  limit: number,

  [name: string]: any,
}

const getBase64 = (file) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
};

const UploadButton = () => (
  <div>
    <PlusOutlined />
    <div className="ant-upload-text">Upload</div>
  </div>
);

const imageToUI = (images) => map(images, (item, index) => ({
  uid: `${item.createdTime}-${index}`,
  name: item.originalname,
  status: 'done',
  url: `${item.filename}`,
  ...item
}));

const uiToImage = (fileList) => map<any, ImageEntity>(fileList, ({ response, originalname, createdTime, filename, mimetype, size }) => {
  if (response) {
    return get(response, 'uploaded[0]'); // : {...otherFields}
  }

  return { createdTime, originalname, filename, mimetype, size };
});

const ImageGallery = forwardRef<Upload, ImageGalleryProps>(({ value, onChange, limit, ...others }, ref) => {

  const [fileList, setFileList] = useState(imageToUI(value));
  const [previewImage, setPreviewImage] = useState();

  const handlePreview = async file => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    setPreviewImage(file.url || file.preview);
  };

  const handleChange = ({ fileList }) => {
    setFileList(fileList);
    onChange(uiToImage(fileList));
  };

  const handleCancel = () => setPreviewImage(undefined);

  return (
    <>
      <Upload
        ref={ref}
        action={`${env.API_ENDPOINT}/upload`}
        name="files"
        listType="picture-card"
        fileList={fileList}
        onPreview={handlePreview}
        onChange={handleChange}
      >
        {
          isNil(limit)
            ? <UploadButton/>
            : fileList.length <= limit && <UploadButton/>
        }
      </Upload>
      <Modal visible={!!previewImage} footer={null} onCancel={handleCancel}>
        <img alt="example" style={{ width: '100%' }} src={previewImage}/>
      </Modal>
    </>
  );
});

export default ImageGallery;
