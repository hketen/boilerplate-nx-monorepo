import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { ApiPOST, selfRedirect, Session } from '../../services';

const Auth = withRouter(({ location }) => {

  const apiCall = async () => {
    try {

      await ApiPOST({ service: '/auth/check' });

    } catch (error) {

      if (error.data && error.data.status === 401) {

        Session.clear();

        selfRedirect('/login');
      }
    }
  };

  useEffect(() => {
    apiCall();
  }, []);

  return null;
});

export default Auth;
