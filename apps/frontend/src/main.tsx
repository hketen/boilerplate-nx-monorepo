import React, { FC, useEffect } from 'react';
import ReactDOM from 'react-dom';
import './styles/style.scss';
import { RootProvider } from './contexts/RootContext';
import { Switch } from 'react-router-dom';
import { CleanLayoutRoute, SideMenuLayoutRoute } from './layouts/layout-routes';
import {
  AccountAddView,
  AccountEditView,
  AccountsView,
  DepositoriesView,
  DepositoryAddView,
  DepositoryEditView,
  Error404View,
  HomeView,
  LoginView,
  ProfileView,
  TransactionTypeAddView,
  TransactionTypeEditView,
  TransactionTypesView
} from './routes';
import { ApiPOST, Session } from './services';
import { NotFoundRoute } from './components';

export const Root: FC = () => {

  useEffect(() => {

    if (!Session.hasToken()) {
      return;
    }

    ApiPOST({ service: '/auth/check' }).catch(console.log);
  }, []);

  return (
    <RootProvider>
      <Switch>
        <CleanLayoutRoute exact path="/login" component={LoginView} isPublic/>

        <SideMenuLayoutRoute exact path="/" component={HomeView}/>

        <SideMenuLayoutRoute exact path="/accounts" component={AccountsView}/>
        <SideMenuLayoutRoute exact path="/accounts/add" component={AccountAddView}/>
        <SideMenuLayoutRoute exact path="/accounts/:id/edit" component={AccountEditView}/>

        <SideMenuLayoutRoute exact path="/settings/depositories" component={DepositoriesView}/>
        <SideMenuLayoutRoute exact path="/settings/depositories/add" component={DepositoryAddView}/>
        <SideMenuLayoutRoute exact path="/settings/depositories/:id/edit" component={DepositoryEditView}/>

        <SideMenuLayoutRoute exact path="/settings/transaction-types" component={TransactionTypesView}/>
        <SideMenuLayoutRoute exact path="/settings/transaction-types/add" component={TransactionTypeAddView}/>
        <SideMenuLayoutRoute exact path="/settings/transaction-types/:id/edit" component={TransactionTypeEditView}/>

        <SideMenuLayoutRoute exact path="/profile" component={ProfileView}/>

        <CleanLayoutRoute exact path="/404" component={Error404View}/>
        <NotFoundRoute/>
      </Switch>
    </RootProvider>
  );
};


ReactDOM.render(
  <Root/>,
  document.getElementById('root')
);
