import React, { ReactComponentElement } from 'react';
import { isWithRoles } from '@boilerplate-nx-monorepo/common-utils';
import {
  DeploymentUnitOutlined,
  HomeOutlined,
  PartitionOutlined,
  ProfileOutlined,
  SettingOutlined,
  UserOutlined
} from '@ant-design/icons';

export interface MenuEntity {
  path: string;
  text: string;
  icon: ReactComponentElement<any>;
  children?: MenuEntity[];
  roles?: string[];
}

/**
 *
 * @param roles ["root", "admin", "user"]
 */
export default (roles?: string[]): MenuEntity[] => {
  const menu: MenuEntity[] = [
    {
      path: '/',
      text: 'Dashboard',
      icon: <HomeOutlined/>
    },
    {
      path: '/accounts',
      text: 'Accounts',
      icon: <UserOutlined/>
    },
    {
      path: '/settings',
      text: 'Ayarlar',
      icon: <SettingOutlined/>,
      children: [
        {
          path: '/depositories',
          text: 'İşlem Noktası',
          icon: <DeploymentUnitOutlined/>
        },
        {
          path: '/transaction-types',
          text: 'İşlem Tipleri',
          icon: <PartitionOutlined/>
        }
      ]
    },
    {
      path: '/profile',
      text: 'Profil',
      icon: <ProfileOutlined/>
    }
  ];

  return menu.filter((menu) => isWithRoles(menu.roles, roles));
};
