import React, { FC, useContext } from 'react';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { chain, map, startsWith } from 'lodash';
import { Layout, Menu } from 'antd';
import menuItems, { MenuEntity } from './menu-items';
import { AuthContext } from '../../contexts/AuthContext';

import './style.scss';

const { Sider } = Layout;

const subPages = /(\/add$|\/edit$|\/stats$)/;

const SideMenuLayout: FC<RouteComponentProps> = ({ children, location }) => {

  const { user: { roles } } = useContext(AuthContext);
  const items = menuItems(roles);

  const { pathname } = location;
  const selectedKey = pathname.replace(subPages, '');
  const openKey = chain(items)
    .find((menu) => menu.path.length > 2 && startsWith(pathname, menu.path))
    .get('path')
    .value();

  return (
    <Layout className="parent-layout side-menu-layout">
      <Sider theme="light" width={220}>
        <div className="logo"/>
        <Menu
          theme="light"
          mode="inline"
          defaultSelectedKeys={[selectedKey]}
          selectedKeys={[selectedKey]}
          defaultOpenKeys={[openKey]}
        >
          {
            map(items, (menu: MenuEntity) => {
              if (menu.children && menu.children.length > 0) {
                return (
                  <Menu.SubMenu
                    key={menu.path}
                    title={
                      <span className="submenu-title-wrapper">
                        {menu.icon}
                        {menu.text}
                      </span>
                    }
                  >
                    {
                      map(menu.children, (childMenu) => (
                        <Menu.Item key={menu.path + childMenu.path}>
                          <Link to={menu.path + childMenu.path}>
                            {childMenu.icon}
                            <span className="nav-text">{childMenu.text}</span>
                          </Link>
                        </Menu.Item>
                      ))
                    }
                  </Menu.SubMenu>
                );
              }

              return (
                <Menu.Item key={menu.path}>
                  <Link to={menu.path}>
                    {menu.icon}
                    <span className="nav-text">{menu.text}</span>
                  </Link>
                </Menu.Item>
              );
            })
          }

        </Menu>
      </Sider>
      <Layout className="content-container">
        {children}
      </Layout>
    </Layout>
  );
};

export default withRouter(SideMenuLayout);
