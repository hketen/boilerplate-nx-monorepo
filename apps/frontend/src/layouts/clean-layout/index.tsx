import React from 'react';

import './style.scss';
import { AppFooter } from '../../components';
import { Layout } from 'antd';

const CleanLayout = ({ children }) => (

  <Layout className="parent-layout">
    {children}
    <AppFooter/>
  </Layout>

);

export default CleanLayout;
