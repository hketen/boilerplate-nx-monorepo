import React, { FC } from 'react';
import { Route, RouteComponentProps, withRouter } from 'react-router-dom';
import LoginRedirect from '../../components/LoginRedirect';
import SideMenuLayout from '../side-menu-layout';
import { isRedirect } from '../../services';

interface SideMenuLayoutRouteProps extends RouteComponentProps {
  isPublic?: boolean,
  component: FC<RouteComponentProps>,

  [propName: string]: any
}

const SideMenuLayoutRoute: FC<SideMenuLayoutRouteProps> = ({ isPublic, component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => {

        if (isRedirect(isPublic)) {
          return (<LoginRedirect/>);
        }

        return (
          <SideMenuLayout>
            <Component {...props} />
          </SideMenuLayout>
        );
      }}
    />
  );
};

export default withRouter(SideMenuLayoutRoute);
