import React, { FC } from 'react';
import { Route, RouteComponentProps, withRouter } from 'react-router-dom';
import { LoginRedirect } from '../../components';
import { isRedirect } from '../../services';
import CleanLayout from '../clean-layout';

interface CleanLayoutRouteProps extends RouteComponentProps {
  isPublic?: boolean,
  component: FC<RouteComponentProps>,

  [propName: string]: any
}

const CleanLayoutRoute: FC<CleanLayoutRouteProps> = ({ isPublic, component, ...rest }) => {

  return (
    <Route
      {...rest}
      render={props => {

        if (isRedirect(isPublic)) {
          return (<LoginRedirect/>);
        }

        return (
          <CleanLayout>
            {React.createElement(component, props)}
          </CleanLayout>
        );
      }}
    />
  );
};


export default withRouter(CleanLayoutRoute);
