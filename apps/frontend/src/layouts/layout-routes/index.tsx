import SideMenuLayoutRoute from './SideMenuLayoutRoute';
import CleanLayoutRoute from './CleanLayoutRoute';

export { CleanLayoutRoute, SideMenuLayoutRoute };
