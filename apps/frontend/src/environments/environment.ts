// This file can be replaced during build by using the `fileReplacements` array.
// When building for production, this file is replaced with `environment.prod.ts`.

export const environment = {
  production: false,
  API_ENDPOINT: 'http://localhost:18010/api',
  PROJECT_NAME: 'LongoWise',
  CustomInputTypes: [
    { value: 'text', label: 'Metin' },
    { value: 'number', label: 'Sayı' },
  ]
};
