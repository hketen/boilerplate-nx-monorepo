export const environment = {
  production: true,
  API_ENDPOINT: 'https://api.publicdomain.com/api',
  PROJECT_NAME: 'Boilerplate Nx Monorepo',
  CustomInputTypes: [
    { value: 'string', label: 'Metin' },
    { value: 'number', label: 'Sayı' }
  ]
};
