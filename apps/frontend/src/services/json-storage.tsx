import {environment as env} from '../environments/environment';

const {PROJECT_NAME} = env;

const tryParse = (item) => {
  try {
    return JSON.parse(item);
  } catch (e) {
    return null;
  }
};


/**
 * only JSON object
 */
class JsonStorage {

  localStorage = window.localStorage;

  getItem(key) {
    return tryParse(this.localStorage.getItem(key));
  }

  setItem(key, value) {
    this.localStorage.setItem(key, JSON.stringify(value));
  }

  extendItem(key, value) {

    const prevValue = this.getItem(key);

    this.setItem(STORAGE_AUTH, {
      ...prevValue,
      ...value
    });
  }

  removeItem(key) {
    this.localStorage.removeItem(key);
  }
}

// Make it singleton.
export default new JsonStorage();

export const STORAGE_AUTH = `${PROJECT_NAME}-auth-user`;
