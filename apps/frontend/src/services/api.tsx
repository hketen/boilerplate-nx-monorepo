import axios from 'axios';
import Session from './session';
import selfRedirect from './self-redirect';
import { environment as env } from '../environments/environment';

const axiosRequest = axios.create({
  withCredentials: true
});

axiosRequest.interceptors.response.use((res) => res, (error) => {
  // Do something with response error
  const { response } = error;
  // response?.status === 401 es7 way.
  if (response && response.status === 401) {
    // Do logout;
    // Redirect to login;
    return Promise.reject(error.response);
  } else if (error.response) {
    // Do something.
    console.log('some API_CLIENT error');
    return Promise.reject(error.response && error.response.data);
  }
  return Promise.reject({
    error: error.message,
    status: (response && response.status) || -1
  });
});

interface RequestOpts {
  service: string,
  payload?: any,
  headers?: any,
  isUpload?: boolean,
  withoutAuth?: boolean,
  useRefreshToken?: boolean,
  options?: any,
}

interface BaseRequestOpts extends RequestOpts {
  method: 'GET' | 'POST' | 'DELETE' | 'PATCH' | 'PUT',
}

const Request = async (opts: BaseRequestOpts) => {

  const {
    method,
    payload,
    service,
    headers,
    withoutAuth = false,
    isUpload,
    useRefreshToken,
    options
  } = opts;

  try {

    const { access_token, refresh_token } = Session.get();

    const config = {
      url: `${env.API_ENDPOINT}${service}`,
      ...(
        method === 'GET'
          ? { params: payload }
          : { data: payload }
      ),
      method,
      headers: {
        'TimezoneOffset': new Date().getTimezoneOffset(),
        'Content-Type': isUpload ? 'multipart/form-data' : 'application/json',
        ...(!withoutAuth && { 'authorization': `Bearer ${useRefreshToken ? refresh_token : access_token || 'unknown'}` }),
        ...headers
      },
      ...options
    };

    const { data } = await axiosRequest(config);
    return data;

  } catch (error) {

    console.log('error: ', typeof error, error);

    if (error.data && error.data.statusCode === 401) {

      if (!useRefreshToken) {
        const refreshTokenData = await ApiRefreshToken();

        Session.set(refreshTokenData);

        return Request({
          ...opts,
          useRefreshToken: false
        });
      }

      Session.clear();

      selfRedirect('/login');

    } else {

      throw error;
    }
  }

};

const ApiRefreshToken = async () => {

  return Request({
    method: 'POST',
    service: '/auth/refresh',
    useRefreshToken: true
  });
};

const ApiGET = async (opts: RequestOpts) => {

  return Request({
    method: 'GET',
    ...opts
  });
};

const ApiPOST = async (opts: RequestOpts) => {

  return Request({
    method: 'POST',
    ...opts
  });
};

const ApiPUT = async (opts: RequestOpts) => {

  return Request({
    method: 'PUT',
    ...opts
  });
};

const ApiPATCH = async (opts: RequestOpts) => {

  return Request({
    method: 'PATCH',
    ...opts
  });
};

const ApiDELETE = async (opts: RequestOpts) => {

  return Request({
    method: 'DELETE',
    ...opts
  });
};

export {
  ApiGET,
  ApiPOST,
  ApiPATCH,
  ApiPUT,
  ApiDELETE
};
