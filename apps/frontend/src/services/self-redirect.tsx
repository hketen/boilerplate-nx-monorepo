/**
 *
 * @param pathname
 */
const selfRedirect = (pathname) => {

  if (window.location.pathname !== pathname) {
    window.location.pathname = pathname;
  }
};

export default selfRedirect;
