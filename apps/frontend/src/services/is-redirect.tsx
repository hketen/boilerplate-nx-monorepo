import Session from './session';

export default (isPublic) => {

  // private route ve giriş yapmamış ise isRedirect=true olacaktır.
  if (!isPublic) {

    if (!Session.hasToken()) {
      return true;
    }
  }

  return false;
}
