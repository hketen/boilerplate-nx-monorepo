import jwtDecoder from 'jwt-decode';
import Storage, { STORAGE_AUTH } from './json-storage';
import { DataResponse, JwtResponse } from '@boilerplate-nx-monorepo/entities';

class Session {

  set({ access_token, refresh_token }: JwtResponse) {

    Storage.extendItem(STORAGE_AUTH, {
      access_token,
      refresh_token,
      user: jwtDecoder(access_token)
    });
  }

  get(): JwtResponse & DataResponse {
    const auth = Storage.getItem(STORAGE_AUTH);

    return { ...auth };
  }

  hasToken() {
    return !!Storage.getItem(STORAGE_AUTH);
  }

  clear() {
    Storage.removeItem(STORAGE_AUTH);
  }

}

// Make it singleton.
export default new Session();
