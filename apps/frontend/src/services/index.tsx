import { ApiDELETE, ApiGET, ApiPATCH, ApiPOST, ApiPUT } from './api';
import isRedirect from './is-redirect';
import selfRedirect from './self-redirect';
import Session from './session';
import JsonStorage, { STORAGE_AUTH } from './json-storage';

export {
  isRedirect,

  ApiDELETE, ApiGET, ApiPATCH, ApiPUT, ApiPOST,
  selfRedirect,
  Session,
  JsonStorage, STORAGE_AUTH
};
