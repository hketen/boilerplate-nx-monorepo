const getWebpackConfig = require('@nrwl/react/plugins/webpack');

module.exports = (config) => {

  return {
    // react config
    ...getWebpackConfig(config),
    node: {
      global: true
    }
  };
};
