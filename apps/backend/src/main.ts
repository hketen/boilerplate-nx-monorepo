/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { UnHandleRejection } from '@boilerplate-nx-monorepo/backend-utils';
import { NestFactory } from '@nestjs/core';
import { BadRequestException, ValidationPipe } from '@nestjs/common';
import { environment as env } from './environments/environment';
import { AppModule } from './app.module';

const { PORT } = env;

UnHandleRejection();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors({
    credentials: true,
    origin: true
  });

  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);

  app.useGlobalPipes(
    new ValidationPipe({
      forbidNonWhitelisted: true,
      whitelist: true,
      exceptionFactory: (errors): BadRequestException =>
        new BadRequestException(errors)
    })
  );

  await app.listen(PORT, () => {
    console.log('Listening at http://localhost:' + PORT + '/' + globalPrefix);
  });
}

bootstrap();
