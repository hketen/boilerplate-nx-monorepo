import { BadRequestException, Body, Controller, Get, Post } from '@nestjs/common';
import { UserService } from '../services/user/user.service';
import { Auth, AuthToken, CurrentUser } from '@boilerplate-nx-monorepo/backend-utils';
import { omit } from 'lodash';

@Controller('auth')
export class AuthController {

  constructor(
    private userService: UserService
  ) {
  }

  @Post('login')
  async login(
    @Body('email') email,
    @Body('password') password
  ) {

    return this.userService.login(email, password);
  }

  @Post('refresh')
  async refresh(
    @AuthToken() refresh_token
  ) {

    if (!refresh_token) {
      throw new BadRequestException('`authorization` field not found in header.');
    }

    return this.userService.resign(refresh_token);
  }

  @Post('register')
  async register(
    @Body('email') email,
    @Body('password') password,
    @Body('passwordRepeat') passwordRepeat
  ) {

    if (password !== passwordRepeat) {

      throw new BadRequestException('Password not equals.');
    }

    await this.userService.register(email, password);

    return {
      success: true
    };
  }

  @Auth()
  @Get('me')
  async me(@CurrentUser() user) {

    return omit(user, ['password', '__v']);
  }

  @Auth()
  @Post('check')
  async check() {
    // empty
  }
}
