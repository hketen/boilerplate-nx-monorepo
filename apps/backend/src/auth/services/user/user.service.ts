import { BadRequestException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { User } from '@boilerplate-nx-monorepo/schemas';
import { JwtService } from '@nestjs/jwt';
import { environment as env } from '../../../environments/environment';
import { JwtPayload, JwtResponse } from '@boilerplate-nx-monorepo/entities';
import { isString } from '@nestjs/common/utils/shared.utils';

const { expiresIn, refreshExpiresIn } = env.Jwt;

@Injectable()
export class UserService {

  constructor(
    private readonly jwtService: JwtService,
    @InjectModel(User) private model: ReturnModelType<typeof User>
  ) {
  }


  async findById(id: string): Promise<User> {

    return this.model
      .findOne({ _id: id });
  }

  async login(email: string, password: string): Promise<JwtResponse> {

    const user = await this.model
      .findOne({ email, password });

    if (!user) {

      throw new NotFoundException('User not found.');
    }

    return this.sign({
      userId: user._id,
      email: user.email,
      fullName: user.fullName,
      roles: user.roles
    });
  }

  async resign(refresh_token: string): Promise<JwtResponse> {

    try {

      const { access_token } = this.jwtService.verify(refresh_token);

      const payload = this.decode(access_token);

      return this.sign(payload);

    } catch (e) {

      throw new UnauthorizedException();
    }
  }

  async register(email: string, password: string) {

    const user = await this.model.findOne({ email });

    if (user) {
      throw  new BadRequestException('User defined.');
    }

    return this.model.create({ email, password });
  }

  private async sign(payload: JwtPayload | { [key: string]: any }): Promise<JwtResponse> {

    const access_token = this.jwtService.sign(payload, {
      expiresIn
    });

    const refresh_token = this.jwtService.sign({ access_token }, {
      expiresIn: refreshExpiresIn
    });

    return {
      access_token,
      refresh_token
    };
  }

  private decode(access_token: string): JwtPayload {
    const payload = this.jwtService.decode(access_token);

    if (!payload || isString(payload)) {
      throw new BadRequestException('`payload` parse error');
    }

    return {
      userId: payload.userId,
      email: payload.email,
      fullName: payload.fullName,
      roles: payload.roles
    };
  }

}
