import { Module } from '@nestjs/common';
import { UserService } from './services/user/user.service';
import { environment as env } from '../environments/environment';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth/auth.controller';
import { TypegooseModule } from 'nestjs-typegoose';
import { User } from '@boilerplate-nx-monorepo/schemas';

const { secret, expiresIn } = env.Jwt;

@Module({
  imports: [
    TypegooseModule.forFeature([
      User
    ]),
    JwtModule.register({
      secret,
      signOptions: {
        expiresIn
      }
    })
  ],
  providers: [UserService],
  controllers: [AuthController],
  exports: [
    JwtModule,
    TypegooseModule
  ]
})
export class AuthModule {
}
