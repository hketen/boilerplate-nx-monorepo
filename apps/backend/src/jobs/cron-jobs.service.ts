import fs from 'fs';
import { promisify } from 'util';
import notepack from 'notepack.io';
import moment from 'moment-timezone';
import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { InjectModel } from 'nestjs-typegoose';
import { User } from '@boilerplate-nx-monorepo/schemas';
import { ReturnModelType } from '@typegoose/typegoose';
// import { environment as env } from '../environments/environment';

// const { Cron: { active } } = env;

const readFileAsync = promisify(fs.readFile);
const writeFileAsync = promisify(fs.writeFile);

@Injectable()
export class CronJobsService {

  constructor(
    @InjectModel(User) private user: ReturnModelType<typeof User>
  ) {
  }

  @Cron('0 2 * * *')
  async backup() {

    if (!fs.existsSync('backups')) {
      fs.mkdirSync('backups');
    }

    const [
      users
    ] = await Promise.all([
      this.user.find().lean()
    ]);


    console.log("users.length", users.length)
  }

}
