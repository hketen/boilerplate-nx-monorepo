export const environment = {
  PORT: 18000,
  production: true,
  MONGO_URL: '..............',
  MONGO_DEBUG: false,

  Cron: {
    active: true
  },

  EmailRegExp: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,

  Jwt: {
    secret: 'KNS+ Token Generator boilerplate-nx-monorepo',
    expiresIn: 15 * 60, // 15 min
    refreshExpiresIn: 60 * 60 * 24 // 1 day
  }
};
