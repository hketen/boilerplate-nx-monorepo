export const environment = {
  PORT: 18010,
  production: false,
  MONGO_URL: '.....',
  MONGO_DEBUG: false,

  Cron: {
    active: false
  },

  EmailRegExp: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,

  Jwt: {
    secret: 'Hakan Keten Token Generator boilerplate-nx-monorepo',
    expiresIn: 15 * 60 * 60, // 15 hours
    refreshExpiresIn: 60 * 60 * 24 * 25 // 15 days
  }
};
