import { Injectable } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { ReturnModelType } from '@typegoose/typegoose';
import { Account } from '@boilerplate-nx-monorepo/schemas';
import { AccountDto, AccountEntity } from '@boilerplate-nx-monorepo/entities';

@Injectable()
export class AccountService {
  constructor(
    @InjectModel(Account) private account: ReturnModelType<typeof Account>
  ) {
  }

  async getAll() {
    return this.account
      .find()
      .sort({ title: 1 })
      .lean();
  }

  async getOne(filter: AccountDto | AccountEntity | Account) {
    return this.account.findOne({ ...filter }).lean();
  }

  async getOneById(_id: string) {
    return this.account.findOne({ _id }).lean();
  }

  async create(
    account: AccountDto | AccountEntity | Account
  ): Promise<Account> {
    return this.account.create(account);
  }

  async update(
    filter: AccountDto | AccountEntity | Account,
    payload: AccountDto | AccountEntity | Account
  ) {
    await this.account.findOneAndUpdate({ ...filter }, { ...payload });

    return this.getOne(filter);
  }

  async removeById(_id: string) {

    return this.account.deleteOne({ _id });
  }
}
