import { Body, Controller, Delete, Get, Param, Patch, Post, Put } from '@nestjs/common';
import { AccountService } from '../../services/account/account.service';
import { AccountEntity } from '@boilerplate-nx-monorepo/entities';

@Controller('accounts')
export class AccountsController {

  constructor(
    private accountService: AccountService
  ) {
  }

  @Get()
  getAll() {

    return this.accountService.getAll();
  }

  @Get(':id')
  getOne(@Param('id') accountId) {

    return this.accountService.getOneById(accountId);
  }

  @Post()
  create(@Body() account: AccountEntity) {

    return this.accountService.create(account);
  }

  @Put(':id')
  @Patch(':id')
  update(@Param('id') accountId, @Body() payload: AccountEntity) {

    return this.accountService.update({ _id: accountId }, payload);
  }

  @Delete(':id')
  async remove(@Param('id') accountId) {

    await this.accountService.removeById(accountId);

    return { success: true };
  }


}
