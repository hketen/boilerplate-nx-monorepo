import { Module } from '@nestjs/common';
import { AccountsController } from './controllers/accounts/accounts.controller';
import { AccountService } from './services/account/account.service';
import { TypegooseModule } from 'nestjs-typegoose';
import { Account } from '@boilerplate-nx-monorepo/schemas';

@Module({
  imports: [
    TypegooseModule.forFeature([
      Account
    ])
  ],
  controllers: [AccountsController],
  providers: [AccountService],
  exports: [TypegooseModule]
})
export class AccountsModule {
}
