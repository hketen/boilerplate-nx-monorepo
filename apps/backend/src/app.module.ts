import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import * as mongoose from 'mongoose';
import { environment as env } from './environments/environment';
import { TypegooseModule } from 'nestjs-typegoose';
import { AuthModule } from './auth/auth.module';
import { CronJobsService } from './jobs/cron-jobs.service';
import { AccountsModule } from './accounts/accounts.module';

const { MONGO_URL, MONGO_DEBUG } = env;

mongoose.set('debug', MONGO_DEBUG);

@Module({
  imports: [
    TypegooseModule.forRoot(MONGO_URL, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true
    }),
    ScheduleModule.forRoot(),
    AuthModule,
    AccountsModule
  ],
  controllers: [],
  providers: [
    CronJobsService
  ]
})
export class AppModule {
}
